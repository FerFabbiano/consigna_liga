#language: es
Característica: Consulta de equipo
  
  @wip
  Escenario: c1 - Consulta de equipo con 2 arqueros y 30 delanteros sin potencial
    Dado que existe el equipo "Los Malos"
    Y que tiene 2 arqueros con potencial 0
    Y que tiene 30 delanteros con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 30
    Y poder defensivo es 2

  @wip
  Escenario: c2 - Consulta de equipo con 2 arqueros y 30 delanteros con potencial
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 8 delanteros con potencial 4
    Y que tiene 12 mediocampistas con potencial 3
    Y que tiene 10 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 112
    Y poder defensivo es 116      

  @wip
  Escenario: elmuro1 - Consulta de equipo con 31 jugadores
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 8 delanteros con potencial 4
    Y que tiene 11 mediocampistas con potencial 3
    Y que tiene 10 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces el estado es INCOMPLETO

  @wip
  Escenario: elmuro2 - Consulta de equipo con 32 jugadores
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 8 delanteros con potencial 4
    Y que tiene 12 mediocampistas con potencial 3
    Y que tiene 10 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces el estado es COMPLETO
    
  @wip
  Escenario: elmuro3 - Consulta de equipo con 3 jugadores fichados
    Dado que existe el equipo "Los Buenos"
    Y que tiene 3 arqueros con valor de mercado 100
    Entonces el presupuesto ejecutado es 300
